package conversaodetipos;

import java.util.Arrays;

public class Principal {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
	       String texto = "TSI em a��o";
	       
	       Util util = new Util();
	       byte[] dados = util.convertStringParaArrayBytes(texto);
	       
	       System.out.println(Arrays.toString(dados));
	       
	       String textoOriginal = util.convertArrayBytesParaString(dados);
	       System.out.println("O texto �: "+textoOriginal);
	       
	       byte[] arrDados = util.convertIntParaArrayByte(2147483647); // m�ximo: 2147483648
	       System.out.println(Arrays.toString(arrDados));
	       
	       int valor = util.convertArrayParaInt(arrDados);
	       System.out.println(valor);

	}

}
