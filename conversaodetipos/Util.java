package conversaodetipos;

public class Util {
    public byte[] convertStringParaArrayBytes(String msg) throws Exception {
        byte[] meuArray = msg.getBytes("UTF-8");
        
        return meuArray;
    }
    
    public String convertArrayBytesParaString(byte[] dados) throws Exception {
        String meuTexto = new String(dados, "UTF-8");
        return meuTexto;
    }
    
    public byte[] convertIntParaArrayByte(int i){
        byte[] meuArray = new byte[4];
        meuArray[3] = (byte)(i & 0xFF);
        meuArray[2] = (byte)((i >> 8) & 0xFF);
        meuArray[1] = (byte)((i >> 16) & 0xFF);
        meuArray[0] = (byte)((i >> 24) & 0xFF);
        
        return meuArray;
    }
    
    public int convertArrayParaInt(byte[] dados){
        int ret = dados[3] & 0xFF | (dados[2] & 0xFF) << 8 | (dados[1] & 0xFF) << 16 | (dados[0] & 0xFF) << 24;
        return ret;
    }

}
